var switchSelected = function(currentSelected,selected) {
	currentSelected.css('color','black');
	currentSelected.removeClass('current');

	selected.css('color','#D2B96B');
	selected.addClass('current');

	selected.parent().prev().text(selected.text());
}

var performancesWithTotalsEndpoint = location.protocol+"//api.cfregisters.org/performances_with_totals";
var playsEndpoint = location.protocol+"//api.cfregisters.org/plays";

var playsCache = null

var fetchPlays = function() {
	return new Promise(function(cb, reject) {
		if (playsCache != null) {
			cb(playsCache); return;
		}
		$.getJSON(playsEndpoint,function(res) {
			var g = _.chain(res).groupBy('id')
			.mapObject(function(val, key) { return val[0]; })
			.value();		
			playsCache = g;
			cb(g);
		});
	});
}

var fetchPerformances = function (year) {
	return new Promise(function(cb, reject){
		var args = "?date=gte."+year+"-01-01&date=lt."+(parseInt(year)+1)+"-01-01";

		$.getJSON(performancesWithTotalsEndpoint+args, function (res) {
				cb(res);
		});
	});
}

var fetchTopPerformances = function (year) {
	return fetchPerformances(year).then(function(res) {
		var grouped = _.chain(res).groupBy('title')
		.mapObject(function(val, key) {
			return _.chain(val).map('total').reduce(function(sum, a) { return sum + (a != null ? Math.round(a/2) : 0)} ).value()
		})
		.pairs()
		.filter(function(val) {return (val[0] != "null");})
		.sortBy(function(val) {return -val[1]})
		.value();

		return grouped;
	});
}

var fetchTopAuthors = function (year) {
	return Promise.all([fetchPerformances(year), fetchPlays()]).then(function(res) {
		var perf = res[0];
		var plays = res[1];
		
		var res = _.chain(perf)
		.map(function(elem) {
			elem['author'] = plays[elem.id].author;
			return elem;
		});
		
		res = res.groupBy('author')		//sum by title
		.mapObject(function(val, key) {
			return _.chain(val).map('total').reduce(function(sum, a) { return sum + (a != null ? Math.round(a/2) : 0)} ).value()
		})
		.pairs()
		.filter(function(val) {return (val[0] != "null");})
		.sortBy(function(val) {return -val[1]})
		.value();
		
		return res;		
	}); 
}

console.log("loaded");

var main = function() {

	// INITIATE YEAR DROPDOWN
	for (i=1680; i<1790; i++) {
		var year = $("<a class='dropdown-choice'></a>").text(i.toString());
		$('#year-dropdown').append(year);
		if (i.toString() == $('#year-dropdown').prev().text()) {
			year.css('color','#D2B96B');
			year.addClass('current');
		}
	}


	// CHOICE HANDLERS
	$("#year-dropdown a.dropdown-choice").click(function() {
		var currentSelected = $("#year-dropdown a.dropdown-choice.current");
		var selected = $(this);
		switchSelected(currentSelected,selected);
	});

	$("#title-dropdown a.dropdown-choice").click(function() {

		var currentSelected = $("#title-dropdown a.dropdown-choice.current");
		var selected = $(this);
		switchSelected(currentSelected,selected);
	});

	$("#superlative-dropdown a.dropdown-choice").click(function() {
		var currentSelected = $("#superlative-dropdown a.dropdown-choice.current");
		var selected = $(this);
		switchSelected(currentSelected,selected);
	});

	// ABOUT THIS AWARD BUTTON

	$(".help-button").click(function() {
		$('.about-award').fadeIn(600);
		$('.about-award').addClass('visible');

		$('.award-builder').css('opacity','0.15');
		$('.award-builder').css('pointer-events','none');

		var superlative = $("#superlative-dropdown a.dropdown-choice.current").text();
		if ((superlative == "BEST") || (superlative == "WORST")) {
			var title = $("#title-dropdown a.dropdown-choice.current").text();
			if (title == "PLAY") {
				$("p#best-play").show();
				$("p#best-play").addClass('visible');
			} else if (title == "THEATRE") {
				$("p#best-theatre").show();
				$("p#best-theatre").addClass('visible');
			} else {
				$("p#other-award").show();
				$("p#other-award").addClass('visible');
			}
		} else {
			$("p#other-award").show();
			$("p#other-award").addClass('visible');
		}
	});

	$(".close-button").click(function() {
		if ($('.about-award').hasClass('visible')) {
			$('.about-award').hide();
			$('.about-award').removeClass('visible');

			$('.award-builder').css('opacity','1.0');
			$('.award-builder').css('pointer-events','auto');

			$('div.about-award .visible').hide();
			$('div.about-award .visible').removeClass('visible');
		}
	});


	// GO BUTTON
	$(".go-button").click(function() {
		var superlative = $("#superlative-dropdown a.dropdown-choice.current").text();
		var title = $("#title-dropdown a.dropdown-choice.current").text();
		var year = $("#year-dropdown a.dropdown-choice.current").text();

		$('.award-builder').fadeOut(function() {
			$('#loadingSpinner').fadeIn();
		});

		// CUSTOMIZE ANNOUNCER
		$('.award-presenter #announcer').html('And the <span>'+superlative+' '+title+' of '+year+' AWARD'+'</span> goes to...');
		$('.award-presenter #announcer span').css('font-family','"Dosis",sans-serif');
		$('.award-presenter #announcer span').css('font-weight','700');
		$('.award-presenter #announcer span').css('color','#D2B96B');

		// CUSTOMIZE WINNER
		$('.award-presenter #winner').text(title+' WINNER HERE');

		// CUSTOMIZE NOMINEES

		var promise;
		if (title.toLowerCase() == "play") {
			promise = fetchTopPerformances(year);
		} else {
			promise = fetchTopAuthors(year);
		}

		decorateTopPlays(promise, (superlative == "BEST" ? true : false), function(performances) {
			setupPieChart(_.chain(performances).first(10).map(function(val, idx) { var n = val.slice(); n.push(idx); return n }).value());

			$('#loadingSpinner').fadeOut(function() {
				$('.award-presenter').fadeIn();
			});

		});
	});

	// AWARD PRESENTER BACK BUTTON
	$('.back-button').click(function() {
		$('.award-presenter').hide();
		$('.award-builder').show();
	});
}

function decorateTopPlays(promise, best, cb) {
	promise.then(function(performances) {
		if (!best) performances = performances.reverse();
		var first = performances[0];
		$("#winner").text(first[0]);
		$("#winner-grossed").text("Grossed: £"+numberWithCommas(first[1]));

		for (var i = 1; i < 11; i++) {
			$("#nominee-list ol li:nth-child("+i+") span").text(performances[i-1][0]+" - £"+numberWithCommas(performances[i-1][1]));
		}

		cb(performances);
	});
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(document).ready(main());
