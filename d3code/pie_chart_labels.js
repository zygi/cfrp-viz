// Adapted from http://bl.ocks.org/mbostock/3887193

function setupPieChart(data) {
  // console.log(data)
  // console.log($("svg").parent().width())
  // console.log($("svg").parent().height())

  var width = 500,
      height = 450,
      radius = Math.min(width, height) / 2;

  var color = d3.scale.linear()
      .domain([0, 9])
      .range(["#6E580F", "#D2B96B"]);

  // console.log(color(data[0][2]))

  var arc = d3.svg.arc()
      .outerRadius(radius - 10)
      .innerRadius(radius - 150);

  var pie = d3.layout.pie()
      .sort(function(d1, d2) { return d3.ascending(d1[2], d2[2]) })
      .value(function(d) { return d[1] });

  var svg = d3.select("svg")
      .attr("width", width)
      .attr("height", height)
    .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


  var g = svg.selectAll(".arc")
      .data(pie(data))
    .enter().append("g")
      .attr("class", "arc");

  g.append("path")
      .attr("d", arc)
      .style("fill", function(d) { return color(d.data[2]); });

  g.append("text")
      .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
      .attr("dy", ".35em")
      .attr("fill", "white")
      .style("font-family", '"Dosis", sans-serif')
      .style("font-size", '25px')
      .text(function(d) { return (parseInt(d.data[2]) + 1)+"."; });
}

function type(d) {
  d.population = +d.population;
  return d;
}
